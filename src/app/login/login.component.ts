import { Component } from '@angular/core';
import { SignUp } from 'src/Model/SignUp';
import { Login } from 'src/Model/login';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  login: Login;
  signUp: SignUp;
  users: SignUp[] = [];

  constructor() {
    this.login = new Login();
    this.signUp = new SignUp();
  }
  onSubmit() {
    this.users.map((user) => {
      // console.log(user);

      if (
        user.password == this.login.password &&
        user.email == this.login.email
      ) {
        Swal.fire('Hii !! Mr. ' + user.name, 'Login Successfully', 'success');
        return;
      } else {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Email OR Password wrong!',
          footer: '<b>Try Agine</b>',
        });
      }
    });
  }

  SubmitSignUp() {
    Swal.fire({
      position: 'top-end',
      icon: 'success',
      title: 'Registered Successfully',
      showConfirmButton: false,
      timer: 1500,
    })
      .then(() => {
        this.users.push(this.signUp);
      })
      .then(() => {
        // console.log(this.users);

        let checkBox = document.getElementById('chk') as HTMLInputElement;
        checkBox.checked = true;
      });
  }
}
